import glob
import os
import dill
import scipy
import uvicorn
import subprocess
import pandas as pd
import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel


app = FastAPI()

list_of_files = glob.glob('../data/model/*.pkl')
last_model = max(list_of_files, key=os.path.getctime)
print(last_model)

with open(last_model, 'rb') as file:
    model = dill.load(file)


class Form(BaseModel):
    id: int
    Year: int
    Month: str
    State: str
    Location: str
    Food: str
    Species: str
    Serotype_Genotype: str
    Status: str


class Form(BaseModel):
    id: int
    Year: int
    Month: str
    State: str
    Location: str
    Food: str
    Species: str
    Serotype_Genotype: str
    Status: str


class Prediction(BaseModel):
    id: int
    pred_illnesses: int


class Auth(BaseModel):
    token: str


@app.get('/status')
def status():
    return 'OK'


@app.get('/version')
def version():
    return model['metadata']


@app.post('/train')
def train(form: Auth):
    true_token = 'Tr94WeaTwMKYb8PptoZ/jP2R8DUNIPE9sE9d8/-psbre7NC3wSAc-Yozh16I88YH'
    if form.token == true_token:
        subprocess.run('python pipeline.py', shell=False)
        return version()
    else:
        return 'Access denied'


@app.post('/predict', response_model=Prediction)
def predict(form: Form):
    df = pd.DataFrame.from_dict([form.dict()]).rename(columns={'Serotype_Genotype': 'Serotype/Genotype'})
    ohe = model['onehotencoder']
    X = pd.DataFrame(ohe.transform(df.drop(columns='id')), columns=ohe.get_feature_names_out(), index=df['id'])
    csr = scipy.sparse.csr_matrix(X)
    y = model['model'].predict(csr)

    return {
        'id': form.id,
        'pred_illnesses': np.round(y)
    }


if __name__ == "__main__":
    uvicorn.run(app, port=8000, host='0.0.0.0')
