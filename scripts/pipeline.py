from datetime import datetime

import dill
import scipy
import pandas as pd
import numpy as np
import lightgbm as lgb
import warnings

from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import r2_score

warnings.filterwarnings('ignore')


def prepare_data(df):
    # fill unknowns
    df = df.drop(['Ingredient', 'Hospitalizations', 'Fatalities'], axis=1)
    df.loc[df['Location'].isna(), 'Location'] = 'Unknown'
    df.loc[df['Serotype/Genotype'].isna(), 'Serotype/Genotype'] = 'Unknown'
    df.loc[df['Food'].isna(), 'Food'] = 'Unknown'
    df.loc[df['Species'].isna(), 'Species'] = 'Unknown'
    df.loc[df['Status'].isna(), 'Status'] = 'Unknown'

    # explode some features
    feature_to_explode = ['Food', 'Species', 'Status', 'Location', 'Serotype/Genotype']
    for feature in feature_to_explode:
        df[feature] = df[feature].astype(str).apply(lambda x: x.split(sep=';'))
        df = df.explode(column=feature)
        df[feature] = df[feature].str.strip()

    # separate features and target
    X = df.drop(['Illnesses'], axis=1)
    y = df['Illnesses']

    # encode data
    ohe = OneHotEncoder(sparse_output=False, max_categories=100)
    X_encoded = pd.DataFrame(ohe.fit_transform(X), columns=ohe.get_feature_names_out(), index=X.index)

    return X_encoded, y, ohe


def train_model(X, y):
    # split and convert to sparse matrix
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=12)
    csr_train = scipy.sparse.csr_matrix(X_train)
    csr_test = scipy.sparse.csr_matrix(X_test)

    # define model
    model = lgb.LGBMRegressor(random_state=12, n_jobs=-1)

    # params for search
    param_grid = {'num_leaves': np.arange(70, 201, 10),
                  'boosting_type': ['gbdt', 'dart', 'rf']}

    # define gridsearch instance
    gs = GridSearchCV(estimator=model, param_grid=param_grid,
                      cv=3, scoring='r2', n_jobs=-1)

    # train models
    gs.fit(csr_train, y_train)

    best_model = gs.best_estimator_
    best_score = r2_score(y_test, best_model.predict(csr_test))

    return best_model, best_score


def main():
    dataframe = pd.read_csv('../data/outbreaks.csv')
    X, y, ohe = prepare_data(dataframe)
    model, score = train_model(X, y)
    time = datetime.now()

    with open(f'../data/model/lgb_{time.strftime("%Y%m%d%H%M")}.pkl', 'wb') as file:
        dill.dump({
            'model': model,
            'onehotencoder': ohe,
            'metadata': {
                'Author': 'Vladimir Bocharov',
                'Version': '1.0',
                'Date': time.strftime("%Y.%m.%d_%H:%M")
            }
        }, file)


if __name__ == "__main__":
    main()
