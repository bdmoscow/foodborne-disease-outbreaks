# Foodborne Disease Outbreaks

This is a test task for Longevity InTime AI powered company.

The idea is to train model to predict how many Illnesses will be produced from different cases which are depend of year, month, state, location, food, etc, and figure out which feature affects more on Illnesses.

The dataset was picked from https://www.kaggle.com/datasets/cdc/foodborne-diseases which was provided from CDC.


## Files structure

/

Foodborne_disease_outbreaks.ipynb - jupyter notebook with main study


data/

* model/ - saved models
* outbreaks.csv - dataset
* test_sample_*.json - samples for testing API


scripts/

* API.py - starts rest api
* pipeline.py - pipeline script


pictures/

* fig_1.png - bar plot with feature importances


## Encoding data

The dataset was processed with filling NaNs and encoded by OneHotEncoder.


## Results

We've got trained model which can predict amount of illnesses with some features given and we know what kind of features make more impact on illnesses. This is an excellent result for further research in this direction.


